# start from official image
FROM python:3.8

# location
RUN mkdir -p /opt/services/job/src
WORKDIR /opt/services/job/src

# project files and dependences
COPY . /opt/services/job/src
RUN pip install pipenv && pipenv install --system

# expose the port 8000
EXPOSE 8000

# default command
CMD ["gunicorn", "--chdir", "job", "--bind", ":8000", "job.wsgi:application"]
