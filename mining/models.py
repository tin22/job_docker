from django.db import models
from django.utils import timezone
from django.urls import reverse


class ActiveManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(status=True)


class Advert(models.Model):
    organization = models.CharField(max_length=60)
    post = models.CharField(max_length=60)
    slug = models.SlugField(max_length=120, unique='pk', default='')
    text = models.TextField()
    salary = models.TextField(max_length=60, null=True)
    person = models.CharField(max_length=60)
    phone = models.CharField(max_length=60)
    pub_date = models.DateField(blank=True, null=True)
    status = models.BooleanField(default=True)

    objects = models.Manager()  # Stanart manager
    active = ActiveManager()  # New manager

    class Meta:
        ordering = ('-pk',)

    def __str__(self):
        return f'{self.organization} {self.post}'

    def get_absolute_url(self):
        return reverse('mining:advert_detail', args=[self.slug, ])


class Activity(models.Model):
    advert = models.ForeignKey(Advert, on_delete=models.CASCADE, related_name='actions')
    timestamp = models.DateTimeField(default=timezone.now)  # auto_now_add=True, 
    action = models.TextField()

    class Meta:
        verbose_name_plural = 'Activties'

    def __str__(self):
        return f"{self.timestamp.strftime('%d.%m %H:%M')} {self.action}"

