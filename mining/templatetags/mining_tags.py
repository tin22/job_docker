import markdown
from django import template
from django.utils.safestring import mark_safe

from ..models import Advert

register = template.Library()


@register.simple_tag
def total_advert():
    return Advert.active.count()


@register.filter(name='markdown')
def markdown_format(text):
    return mark_safe(markdown.markdown(text))

