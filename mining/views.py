from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView

from mining.models import Advert
from .forms import ActionForm


class AdvertListView(ListView):
    queryset = Advert.active.all()
    ordering = ('-pk')
    context_object_name = 'adverts'
    template_name = "mining/adverts/list.html"


class AdvertDetailView(DetailView):  # Допустим, так
    pass


def advert_detail(request, advert):
    advert = get_object_or_404(Advert, slug=advert)
    # List of actions for this advert
    actions = advert.actions.all()

    new_action = None

    if request.method == 'POST':
        # A new action was posted
        action_form = ActionForm(data=request.POST)
        if action_form.is_valid():
            # Create action object, but don't save to database yet.
            new_action = action_form.save(commit=False)
            # Assign the current advert to the action
            new_action.advert = advert
            # save the action to database
            new_action.save()
    else:
        action_form = ActionForm()
    return render(request, "mining/adverts/detail.html",
                  {'advert': advert,
                   'actions': actions,
                   'new_action': new_action,
                   'action_form': action_form})
