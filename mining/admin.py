from django.contrib import admin

from .models import Advert, Activity


class ActivityInline(admin.TabularInline):
    model = Activity
    extra = 0


@admin.register(Advert)
class AdvertAdmin(admin.ModelAdmin):
    list_display = ('organization', 'post', 'salary', 'pub_date',)
    prepopulated_fields = {'slug': ('organization', 'post',)}

    inlines = [ActivityInline]


@admin.register(Activity)
class ActivityAdmin(admin.ModelAdmin):
    list_display = ('timestamp', 'action',)
    ordering = ('-timestamp',)
