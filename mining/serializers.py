from rest_framework import serializers
from .models import Advert, Activity


class ActivitySerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Activity
        fields = ('advert', 'timestamp', 'action')


class AdvertSerializer(serializers.ModelSerializer):

    class Meta:
        model = Advert
        fields = ('__all__')


class AdvertActivitySerializer(serializers.ModelSerializer):
    actions = serializers.StringRelatedField(many=True, read_only=True)

    class Meta:
        model = Advert
        fields = ('organization', 'post', 'actions')
