from django.urls import path

from . import views

app_name = 'mining'

urlpatterns = [
    path('', views.AdvertListView.as_view(), name='advert_list'),
    path('<slug:advert>/', views.advert_detail, name='advert_detail'),
]
