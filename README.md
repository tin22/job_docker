# Приложение для поиска работы. web-версия программы jobmining. 
После клонирования репозитория необходимо:

* запустить докер-контейнер:

``` bash
	$ docker-compose up -d
```

* выполнить миграции:

``` bash
	$ docker-compose exec job ./manage.py makemigrations
	$ docker-compose exec job ./manage.py migrate
```

* создать суперпользователя:

``` bash
	$ docker-compose exec job ./manage.py createsuperuser
```

* собрать статику:

``` bash
	$ docker-compose exec job ./manage.py collectstatic
```

* и сделать так:
``` bash
    $ docker cp static/ f0785fa75f3e:/opt/services/job/
```
(иначе все собирается не как положено)

ID контейнера можно уточнить
``` bash
    $ docker ps
```
(который job_job)

В админке можно добавлять объявления и действия по ним.
