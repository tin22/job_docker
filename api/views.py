from rest_framework import viewsets

from api.serializers import AdvertSerializer
from mining.models import Advert


class AdvertViewSet(viewsets.ModelViewSet):
    queryset = Advert.active.all()
    serializer_class = AdvertSerializer
    lookup_field = 'slug'
