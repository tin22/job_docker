from rest_framework.routers import SimpleRouter

from . import views

router = SimpleRouter()
router.register('adverts', views.AdvertViewSet, basename='adverts')
router.register('', views.AdvertViewSet, basename='adverts')

urlpatterns = router.urls
