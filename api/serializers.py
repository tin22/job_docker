from rest_framework import serializers

from mining.models import Advert, Activity


class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = ('timestamp', 'action')
        ordering = ('-timestamp',)


class AdvertSerializer(serializers.ModelSerializer):
    actions = ActivitySerializer(many=True, required=False)

    class Meta:
        model = Advert
        fields = ('organization', 'post', 'salary', 'slug', 'actions')
